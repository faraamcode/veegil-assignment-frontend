import { FaUser, FaLock } from "react-icons/fa";
import { Link } from "react-router-dom";
import { UseSignUpContext } from "../context/signup.context";
function SignUp() {
  const {
    handleSignup,
    account,
    message,
    handleChange,
    loading,
  } = UseSignUpContext();
  if (account) {
    return (
      <section className="section-center section-notification">
        <h4>{message}</h4>
        <Link to="/" className="btn ">
          login
        </Link>
      </section>
    );
  }
  return (
    <section className="section-center section-signup">
      <h4>Registration form</h4>
      <p>Please fill in the form with the appropriate information</p>
      <p className="error">{message}</p>
      <form action="" className="signup-form" onSubmit={handleSignup}>
        <div className="two-column">
          <div className="form-control-normal">
            <label htmlFor="">surname</label>
            <input
              type="text"
              name="surname"
              id=""
              onChange={handleChange}
              required
            />
          </div>
          <div className="form-control-normal">
            <label htmlFor="">Lastname</label>
            <input
              type="text"
              name="last_name"
              id=""
              onChange={handleChange}
              required
            />
          </div>
        </div>
        <div className="two-column">
          <div className="form-control-normal">
            <label htmlFor="">Phone number</label>
            <input
              type="text"
              name="phone_number"
              id=""
              onChange={handleChange}
              required
            />
          </div>
          <div className="form-control-normal">
            <label htmlFor="gender">gender</label>

            <select name="gender" id="" onChange={handleChange} required>
              <option value="">Select a gender</option>
              <option value="male">Male</option>
              <option value="female">Female</option>
            </select>
          </div>
        </div>
        <div className="two-column">
          <div className="form-control-normal">
            <label htmlFor="">email</label>
            <input
              type="text"
              name="email"
              id=""
              onChange={handleChange}
              required
            />
          </div>
          <div className="form-control-normal">
            <label htmlFor="">Date of birth</label>
            <input
              type="date"
              name="dob"
              id=""
              onChange={handleChange}
              required
            />
          </div>
        </div>
        <div className="two-column">
          <div className="form-control-normal">
            <label htmlFor="">account type</label>
            <select name="account_type" id="" onChange={handleChange} required>
              <option value="">Select Account Type</option>
              <option value="savings">Savings Account</option>
              <option value="current">Current Account</option>
            </select>
          </div>
        </div>
        <div className="form-control-normal">
          <label htmlFor="">next of kin's full name</label>
          <input
            type="text"
            name="next_of_kin_name"
            id=""
            onChange={handleChange}
            required
          />
        </div>
        <div className="two-column">
          <div className="form-control-normal">
            <label htmlFor="">next of kin phone number</label>
            <input
              type="text"
              name="next_of_kin_number"
              id=""
              onChange={handleChange}
              required
            />
          </div>
          <div className="form-control-normal">
            <label htmlFor="">next of kin email</label>
            <input
              type="text"
              name="next_of_kin_email"
              id=""
              onChange={handleChange}
              required
            />
          </div>
        </div>
        <div className="two-column">
          <div className="form-control-normal">
            <label htmlFor="">password</label>
            <input
              type="password"
              name="password1"
              id=""
              onChange={handleChange}
              required
            />
          </div>
          <div className="form-control-normal">
            <label htmlFor="">confirm password</label>
            <input
              type="password"
              name="password2"
              id=""
              onChange={handleChange}
              required
            />
          </div>
        </div>
        <p className="register">
          <input type="checkbox" name="" id="" required />
          accept <a href="">terms and conditions</a>
        </p>
        <input
          type="submit"
          value={loading ? "saving..." : "submit"}
          className="btn signup-btn"
        />
      </form>
    </section>
  );
}

export default SignUp;
