import { FaUser, FaLock } from "react-icons/fa";
import { Link, Redirect } from "react-router-dom";
import { UseLoginContext } from "../context/login.context";
import { useEffect } from "react";

function Login() {
  const {
    handleLogin,
    handleChange,
    message,
    getUser,
    access,
    setMessage,
    loading,
  } = UseLoginContext();
  useEffect(() => {
    let cancel = false;
    if (!cancel) {
      setTimeout(() => {
        getUser();
      }, 3000);
    }
    return () => {
      cancel = true;
    };
  }, []);
  useEffect(() => {
    let cancel = false;
    if (!cancel) {
      setTimeout(() => {
        setMessage("");
      }, 2000);
    }
    return () => {
      cancel = true;
    };
  }, [message]);
  if (access) {
    return <Redirect to="/dashboard" />;
  }
  return (
    <div className="login-wrapper">
      <section className="section-login">
        <h4 className="error-meesage">{message}</h4>
        <form action="" className="login-form" onSubmit={handleLogin}>
          <div className="form-control">
            <span>
              <FaUser className="login-icon" />
            </span>
            <input
              type="text"
              name="account_no"
              id=""
              placeholder="enter your account number"
              onChange={handleChange}
            />
          </div>
          <div className="form-control">
            <span>
              <FaLock className="login-icon" />
            </span>
            <input
              type="password"
              name="password"
              id=""
              placeholder="enter your password"
              onChange={handleChange}
            />
          </div>
          <input
            type="submit"
            className="btn btn-login"
            value={loading ? "logging in..." : "login"}
          />
        </form>
        <p className="register">
          dont have an account <Link to="/register">register here</Link> <br />
          <Link to="/" className="forget-password">
            forget password
          </Link>
        </p>
      </section>
    </div>
  );
}

export default Login;
