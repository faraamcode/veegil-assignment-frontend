import { SideBar } from "../component";
import { FaUser, FaLock } from "react-icons/fa";
import { Link, Route, Redirect, useHistory } from "react-router-dom";
import { useEffect, useState } from "react";
import DepositForm from "../component/deposit.form";
import WithdrawalForm from "../component/withdrawal.form";
import HistoryTable from "../component/History.table";
import TransferForm from "../component/transfer.form";
import Profile from "../component/profile";
import { UseDashboardContext } from "../context/dashboard.context";
import { UseSidebarContext } from "../context/sidebar.context";
import { UseDepositContext } from "../context/deposit.context";
import { UseWithdrawContext } from "../context/withdrawal.context";
import { UseTransferContext } from "../context/transfer.context";
import { UseLoginContext } from "../context/login.context";

// import { useState } from "react";

function Dashboard() {
  // const die = false;
  //   const { access } = UseLoginContext();
  const history = useHistory();
  const { currentMenu, sideBar } = UseSidebarContext();
  const { update } = UseDepositContext();
  const { withdraw } = UseWithdrawContext();
  const { transfer } = UseTransferContext();
  const { access, getUser } = UseLoginContext();
  console.log(currentMenu);
  const { getToken, token, userDetails } = UseDashboardContext();

  useEffect(() => {
    let cancle = false;
    if (!cancle) {
      getUser();
    }
    return () => {
      cancle = true;
    };
  }, []);

  useEffect(() => {
    let cancle = false;
    if (!cancle) {
      setTimeout(() => {
        if (!access) {
          history.push("/");
        }
      }, 3000);
    }
    return () => {
      cancle = true;
    };
  }, []);

  useEffect(() => {
    let isCancelled = false;

    getToken();

    return () => {
      isCancelled = true;
    };
  }, []);

  useEffect(() => {
    let isCancelled = false;
    if (!isCancelled) {
      getToken();
    }
    return () => {
      isCancelled = true;
    };
  }, [update, transfer, withdraw]);

  // useEffect(() => {
  //   getToken();
  // }, [transfer]);

  // useEffect(() => {
  //   getToken();
  // }, [withdraw]);

  // console.log(userDetails);

  return (
    <main>
      {sideBar && <SideBar />}
      <section className="section-center section-dashoard">
        <div className="main-details">
          {currentMenu === "deposit" && <DepositForm />}
          {currentMenu === "withdrawal" && <WithdrawalForm />}
          {currentMenu === "history" && <HistoryTable />}
          {currentMenu === "transfer" && <TransferForm />}
          {currentMenu === "profile" && <Profile />}
        </div>
        <div className="profile-container">
          <div className="img-wrapper">
            <img src="images/bank1.jpg" alt="" className="profile-photo" />
          </div>
          <div className="user-details">
            <h5>Name : {userDetails.fullname ? userDetails.fullname : ""}</h5>
            <p>
              Balance : <span className="naira">N</span>
              {userDetails.balance ? userDetails.balance : ""}
            </p>
          </div>
        </div>
      </section>
    </main>
  );
}

export default Dashboard;
