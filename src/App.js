import logo from './logo.svg'
import './App.css'
import { SignUp, Login, Dashboard } from './pages'
import { TopBar, Hero } from './component'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { SignUpProvider } from './context/signup.context'
import { LoginProvider } from './context/login.context'
import { DashboardProvider } from './context/dashboard.context'
import { SidebarProvider } from './context/sidebar.context'
import { DepositProvider } from './context/deposit.context'
import { WithdrawProvider } from './context/withdrawal.context'
import { HistoryProvider } from './context/history.context'
import { TransferProvider } from './context/transfer.context'

function App () {
  return (
    <>
      <TransferProvider>
        <HistoryProvider>
          <WithdrawProvider>
            <DepositProvider>
              <SidebarProvider>
                <DashboardProvider>
                  <LoginProvider>
                    <SignUpProvider>
                      <Router>
                        <TopBar />
                        <Route exact path='/'>
                          <Login />
                        </Route>
                        <Route path='/dashboard' exact>
                          <Dashboard />
                        </Route>

                        <Route path='/register'>
                          <Hero />
                          <SignUp />
                        </Route>
                      </Router>
                    </SignUpProvider>
                  </LoginProvider>
                </DashboardProvider>
              </SidebarProvider>
            </DepositProvider>
          </WithdrawProvider>
        </HistoryProvider>
      </TransferProvider>
    </>
  )
}

export default App
