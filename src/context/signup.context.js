import React, { useState, useEffect, useContext } from "react";
const url = "https://veegil-bank.herokuapp.com/signup";
const SignUpContext = React.createContext();

const SignUpProvider = ({ children }) => {
  const [loading, setLoading] = useState(false);

  const [formData, setFormData] = useState({});
  const [message, setMessage] = useState("");
  const [account, setAccount] = useState(false);

  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setFormData({ ...formData, [name]: value });
  };
  const handleSignup = (e) => {
    setLoading(true);
    e.preventDefault();
    if (formData.password1 !== formData.password2) {
      return setMessage("password not the same");
    }
    const fullname = `${formData.surname} ${formData.last_name}`;
    const submitData = {
      fullname: fullname,
      phone_number: formData.phone_number,
      email: formData.email,
      date_became_customer: "NOW()",
      username: formData.phone_number,
      password: formData.password1,
      gender: formData.gender,
      date_of_birth: formData.dob,
      passport: "nil",
      next_of_kin_name: formData.next_of_kin_name,
      next_of_kin_email: formData.next_of_kin_email,
      next_of_kin_phone: formData.next_of_kin_number,
      account_type: formData.account_type,
      balance: 0,
      last_login: "NOW()",
    };
    console.log(submitData);

    fetch(url, {
      method: "post",
      body: JSON.stringify(submitData),
      headers: {
        "content-type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((result) => {
        setAccount(result.account);
        setMessage(result.message);
        setLoading(false);
      })
      .catch((err) => console.log(err));
  };
  return (
    <SignUpContext.Provider
      value={{ handleSignup, message, account, handleChange, loading }}
    >
      {children}
    </SignUpContext.Provider>
  );
};

const UseSignUpContext = () => {
  return useContext(SignUpContext);
};

export { SignUpProvider, UseSignUpContext };

// {surname: "Ibrahim", last_name: "Abdulrasaq", phone_number: "49349934", gender: "female", dob: "2021-04-09", …}
// account_type: "savings"
// dob: "2021-04-09"
// email: "abdulrasaqalagbede@gmail.com"
// gender: "female"
// last_name: "Abdulrasaq"
// next_of_kin_email: "rere"
// next_of_kin_name: "ferr"
// next_of_kin_number: "reorier"
// password1: "hjhhhhh"
// password2: "uuuiiiii"
// phone_number: "49349934"
// surname: "Ibrahim"
// __proto__: Object
