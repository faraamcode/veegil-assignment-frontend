import React, { useState, useEffect, useContext } from "react";
import { Redirect } from "react-router-dom";
const url = "https://veegil-bank.herokuapp.com/test";
const DashboardContext = React.createContext();

const DashboardProvider = ({ children }) => {
  const [access, setAccess] = useState(true);
  const [token, setToken] = useState("");
  const [userDetails, setUserDetails] = useState({});
  const getToken = () => {
    const dbtoken = localStorage.getItem("token");
    const userDetail = localStorage.getItem("userdetails");
    setToken(dbtoken);
    setUserDetails(JSON.parse(userDetail));
    // setAccess(true);
    const data = {
      authorization: dbtoken,
    };
    // console.log(data);
    fetch(url, {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "content-type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((result) => {
        console.log(result.access);
      });
  };
  return (
    <DashboardContext.Provider
      value={{ getToken, setToken, setUserDetails, access, token, userDetails }}
    >
      {children}
    </DashboardContext.Provider>
  );
};

const UseDashboardContext = () => {
  return useContext(DashboardContext);
};

export { DashboardProvider, UseDashboardContext };
