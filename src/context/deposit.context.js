import React, { useState, useEffect, useContext } from "react";
import { Redirect } from "react-router-dom";
const url = "https://veegil-bank.herokuapp.com/transaction/deposit";
const url2 = "https://veegil-bank.herokuapp.com/find";
const DepositContext = React.createContext();

const DepositProvider = ({ children }) => {
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState("");
  const [update, setUpdate] = useState(true);
  const [isMessage, setIsMessage] = useState(false);
  const [deposit, setDeposit] = useState({});
  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setDeposit({ ...deposit, [name]: value });
  };
  const getUserDetails = (value) => {
    // console.log(value);
    const data = {
      phone_number: value,
    };
    fetch(url2, {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "content-type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((result) => {
        localStorage.setItem("userdetails", JSON.stringify(result));
        setUpdate(!update);
      });
  };
  const handleDeposit = (value) => {
    setLoading(true);

    const formData = {
      amount: deposit.amount,
      phone_number: deposit.phone_number,
      time_of_transaction: "NOW()",
      transaction_type: "DEPOSIT",
      other_details: deposit.slip_no,
      authorization: deposit.authorization,
    };

    fetch(url, {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "content-type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((result) => {
        setIsMessage(true);
        setMessage(result.message);
        setLoading(false);
      })
      .catch((err) => console.log(err));
    setTimeout(() => {
      getUserDetails(deposit.phone_number);
    }, 2000);
  };

  const handleToken = (tokenValue, accountValue) => {
    setDeposit({
      ...deposit,
      authorization: tokenValue,
      phone_number: accountValue,
    });
  };
  return (
    <DepositContext.Provider
      value={{
        handleDeposit,
        handleChange,
        handleToken,
        message,
        getUserDetails,
        update,
        setMessage,
        loading,
      }}
    >
      {children}
    </DepositContext.Provider>
  );
};
const UseDepositContext = () => {
  return useContext(DepositContext);
};

export { DepositProvider, UseDepositContext };
