import React, { useState, useEffect, useContext } from "react";
import { Redirect } from "react-router-dom";
const url = "https://veegil-bank.herokuapp.com/test";
const SidebarContext = React.createContext();

const SidebarProvider = ({ children }) => {
  const [currentMenu, setCurrentMenu] = useState("profile");
  const [sideBar, setSideBar] = useState(false);
  const handleClick = (value) => {
    setCurrentMenu(value);
    setSideBar(false);
  };
  return (
    <SidebarContext.Provider
      value={{ handleClick, currentMenu, setSideBar, sideBar }}
    >
      {children}
    </SidebarContext.Provider>
  );
};
const UseSidebarContext = () => {
  return useContext(SidebarContext);
};

export { SidebarProvider, UseSidebarContext };
