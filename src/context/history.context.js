import React, { useState, useEffect, useContext } from "react";
import { Redirect } from "react-router-dom";
const url = "https://veegil-bank.herokuapp.com/transaction/history/paginate";
const HistoryContext = React.createContext();

const HistoryProvider = ({ children }) => {
  const [page, setPage] = useState(0);
  const [maxPage, setMaxPage] = useState(5);
  const [history, setHistory] = useState([]);
  const handlePrev = (e) => {
    e.preventDefault();
    if (page < 0) {
      setPage(maxPage);
    } else {
      setPage(page - 1);
    }
  };
  const handleNext = (e) => {
    e.preventDefault();
    if (page > maxPage) {
      setPage(0);
    } else {
      setPage(page + 1);
    }
  };
  const fetchHistory = (value) => {
    const data = {
      phone_number: value,
      page: page,
    };
    fetch(url, {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "content-type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((data) => {
        const { page, result } = data;
        const pagenum = Math.ceil(page / 10) - 1;
        setHistory(result);
        setMaxPage(pagenum);
        console.log(result);
        console.log(maxPage);
      });
    // console.log(data);
  };

  return (
    <HistoryContext.Provider
      value={{ fetchHistory, history, handlePrev, handleNext, page }}
    >
      {children}
    </HistoryContext.Provider>
  );
};
const UseHistoryContext = () => {
  return useContext(HistoryContext);
};

export { HistoryProvider, UseHistoryContext };
