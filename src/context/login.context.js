import React, { useState, useEffect, useContext } from "react";
const url = "https://veegil-bank.herokuapp.com/login";
const LoginContext = React.createContext();

const LoginProvider = ({ children }) => {
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState("");
  const [access, setAccess] = useState(false);
  const [loginData, setLoginData] = useState({});
  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setLoginData({ ...loginData, [name]: value });
  };
  const handleLogin = (e) => {
    e.preventDefault();
    setLoading(true);
    const loginDetails = {
      phone_number: loginData.account_no,
      password: loginData.password,
    };

    fetch(url, {
      method: "post",
      body: JSON.stringify(loginDetails),
      headers: {
        "content-type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((result) => {
        setMessage(result.message);
        console.log(result.access);

        console.log(result);
        localStorage.setItem("token", result.token);
        localStorage.setItem("userdetails", JSON.stringify(result.userDetails));
        localStorage.setItem("access", true);
        setAccess(result.access);
        setLoading(false);
      })
      .catch((err) => console.log(err));
  };
  const getUser = () => {
    const user = localStorage.getItem("access");
    setAccess(user);
  };
  return (
    <LoginContext.Provider
      value={{
        handleLogin,
        access,
        message,
        setMessage,
        setAccess,
        handleChange,
        getUser,
        loading,
      }}
    >
      {children}
    </LoginContext.Provider>
  );
};

const UseLoginContext = () => {
  return useContext(LoginContext);
};

export { LoginProvider, UseLoginContext };
