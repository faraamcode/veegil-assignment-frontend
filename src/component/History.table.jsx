import { useEffect } from "react";
import { UseDashboardContext } from "../context/dashboard.context";
import { UseHistoryContext } from "../context/history.context";
import { Link } from "react-router-dom";
function HistoryTable() {
  const { token, userDetails } = UseDashboardContext();

  const {
    fetchHistory,
    history,
    handlePrev,
    handleNext,
    page,
  } = UseHistoryContext();
  useEffect(() => {
    fetchHistory(userDetails.phone_number);
  }, []);
  useEffect(() => {
    fetchHistory(userDetails.phone_number);
  }, [page]);
  return (
    <section className="history">
      <h3>transactions history</h3>
      <table>
        <thead>
          <tr>
            <th>time of transactions</th>
            <th>type</th>
            <th>amount</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          {history.map((item) => {
            return (
              <tr key={item.id}>
                <td>{item.time_of_transaction}</td>
                <td>{item.transaction_type}</td>
                <td>{item.amount}</td>
                <td>{item.other_details}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <div className="history-buttons">
        <button className="prev-btn btn" onClick={handlePrev}>
          Previous
        </button>
        <button className="next-btn btn" onClick={handleNext}>
          Next
        </button>
      </div>
    </section>
  );
}

export default HistoryTable;
