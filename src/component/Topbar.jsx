import { FaUser, FaLock, FaBars } from "react-icons/fa";
import { Link } from "react-router-dom";
import { SideBar } from ".";
import { UseLoginContext } from "../context/login.context";
import { UseSidebarContext } from "../context/sidebar.context";
function TopBar() {
  const { access, setAccess } = UseLoginContext();
  const { setSideBar, sideBar } = UseSidebarContext();
  const logout = () => {
    if (access) {
      setAccess(false);
      localStorage.setItem("token", "");
      localStorage.setItem("userdetails", "");
      localStorage.setItem("access", "");
    }
  };
  return (
    <header>
      {access && (
        <Link
          to="/dashboard"
          onClick={() => setSideBar(!sideBar)}
          className="toggle-wrapper"
        >
          <FaBars className="toggle-icon" />
        </Link>
      )}

      <section className="section-center section-header">
        <div className="logo-container">
          <h4>Veegil Bank</h4>
        </div>
        <div className="other-header-content">
          <Link to="/" onClick={logout}>
            {access ? "logout" : "account login"}
          </Link>
        </div>
      </section>
    </header>
  );
}

export default TopBar;
