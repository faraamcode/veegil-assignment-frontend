import { FaUser, FaLock } from "react-icons/fa";
import { Link } from "react-router-dom";
function Hero() {
  return (
    <section className="section-hero">
      <img
        src="https://res.cloudinary.com/alagbede4340/image/upload/v1619397352/bank2-min_pcbwng.jpg"
        alt=""
      />

      <div className="hero-text">
        <h4>Veegil Bank</h4>
        <p>our customer first</p>
      </div>
    </section>
  );
}

export default Hero;
