import { FaUser, FaToolbox } from "react-icons/fa";
import { AiOutlineDashboard } from "react-icons/ai";
import { SiContactlesspayment } from "react-icons/si";
import { RiLuggageDepositFill } from "react-icons/ri";
import { Link } from "react-router-dom";
function DepositForm() {
  return (
    <section className="deposit-form">
      <div className="form-title">
        <h4>Money deposit</h4>
      </div>
      <form action="" className="deposit-form">
        <div className="form-control-normal deposit-control">
          <label htmlFor="">Account Number</label>

          <input
            type="text"
            name="surname"
            id=""
            maxLength="10"
            placeholder="account number"
            required
          />
        </div>
        <div className="form-control deposit-control">
          <span className="naira">N</span>
          <input
            type="number"
            name=""
            id=""
            defaultValue="00.00"
            placeholder="00.00"
          />
        </div>
        <input type="submit" value="send" className="btn btn-deposit" />
      </form>
    </section>
  );
}

export default DepositForm;
