import { FaUser, FaToolbox } from "react-icons/fa";
import { AiOutlineDashboard } from "react-icons/ai";
import { SiContactlesspayment } from "react-icons/si";
import { RiLuggageDepositFill } from "react-icons/ri";
import { GoHistory } from "react-icons/go";
import { Link } from "react-router-dom";
import { UseSidebarContext } from "../context/sidebar.context";

function SideBar() {
  const { handleClick, sideBar } = UseSidebarContext();
  return (
    <aside className="sidebar">
      <Link
        to="/dashboard"
        className="link-wrapper"
        onClick={() => handleClick("profile")}
      >
        <AiOutlineDashboard className="link-icon" />
        <p>Dashboard</p>
      </Link>
      <Link
        to="/dashboard"
        className="link-wrapper"
        onClick={() => handleClick("deposit")}
      >
        <RiLuggageDepositFill className="link-icon" />
        <p>Deposit</p>
      </Link>
      <Link
        to="/dashboard"
        className="link-wrapper"
        onClick={() => handleClick("withdrawal")}
      >
        <FaToolbox className="link-icon" />
        <p>Withdrawal</p>
      </Link>
      <Link
        to="/dashboard"
        className="link-wrapper"
        onClick={() => handleClick("transfer")}
      >
        <FaToolbox className="link-icon" />
        <p>Transfer</p>
      </Link>
      <Link
        to="/dashboard"
        className="link-wrapper"
        onClick={() => handleClick("history")}
      >
        <SiContactlesspayment className="link-icon" />
        <p>account history</p>
      </Link>
      <Link
        to="/dashboard"
        className="link-wrapper"
        onClick={() => handleClick("profile")}
      >
        <FaUser className="link-icon" />
        <p>Profile</p>
      </Link>
    </aside>
  );
}

export default SideBar;
