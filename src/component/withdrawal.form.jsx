import { useEffect } from "react";
import { FaUser, FaToolbox } from "react-icons/fa";
import { AiOutlineDashboard } from "react-icons/ai";
import { SiContactlesspayment } from "react-icons/si";
import { RiLuggageDepositFill } from "react-icons/ri";
import { UseWithdrawContext } from "../context/withdrawal.context";
import { UseDashboardContext } from "../context/dashboard.context";
import { Link } from "react-router-dom";
function WithdrawalForm() {
  const { token, userDetails } = UseDashboardContext();
  const {
    handleDeposit,
    handleChange,
    handleToken,
    message,
    getUserDetails,
    setMessage,
    loading,
  } = UseWithdrawContext();
  useEffect(() => {
    // handleSetting("authorization", token);
    handleToken(token, userDetails.phone_number);
  }, []);
  useEffect(() => {
    setTimeout(() => {
      setMessage("");
    }, 2000);
  }, [message]);
  const handleSubmit = (e) => {
    e.preventDefault();
    handleDeposit(userDetails.phone_number);
  };
  return (
    <section className="deposit-form">
      <div className="form-title">
        <h4>Money Withdrawal</h4>
      </div>
      <div className="message-container">
        <h4>{message}</h4>
      </div>
      <form className="deposit-form">
        <div className="form-control-normal deposit-control">
          <label htmlFor="">Slip Number</label>

          <input
            type="text"
            name="slip_no"
            id=""
            maxLength="10"
            placeholder="account number"
            required
            onChange={handleChange}
          />
        </div>
        <div className="form-control deposit-control">
          <span className="naira">N</span>
          <input
            type="number"
            name="amount"
            id=""
            defaultValue="00.00"
            placeholder="00.00"
            onChange={handleChange}
          />
        </div>
        <input
          type="submit"
          value={loading ? "saving..." : "save"}
          className="btn btn-deposit"
          onClick={handleSubmit}
        />
      </form>
    </section>
  );
}

export default WithdrawalForm;
