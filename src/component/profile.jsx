function Profile() {
  return (
    <div className="important-notice">
      <h4>Important notice</h4>
      <p>
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ullam
        consectetur id officiis earum velit atque sint ratione vero quo ducimus,
        reprehenderit, accusantium ex possimus quis vel dolorum molestiae.
        Quibusdam id corrupti dolor ipsa, et iusto culpa cum assumenda,
        consequuntur sapiente voluptates inventore consequatur voluptatibus.
        Cumque voluptas dolores beatae soluta omnis.
      </p>
    </div>
  );
}

export default Profile;
