# VEEGIL ASSIGNMENT SUBMITTED BY IBRAHIM ABDULRASAQ KAYODE

## VEEGIL BANK APPLICATION (REACT)

This is web application written with react js. This frontend will communicate with its backend RESTAPI to perform the following functions

## This application has been hosted on heroku

(link) [https://veegil-bank-frontend.herokuapp.com/]

> creation of account (signup)
> login into the system
> money deposit
> money transfer and withdrawal

# Installation

> > you will clone or download the source code to your computer
> > install the depencies

```
npm install
```

> Start the application

```
npm start
```
